#!/usr/bin/env python3.6

from bs4 import BeautifulSoup
import json
import pycurl
import os
from ark import Ark

DEBUG = False
STORE='_dndtools.net'
Contents = {}


def fetch(url, fname, Prefix='--------'):
    if os.path.exists(fname):
        print(f'{Prefix}> No Clobber: {fname}')
    else:
        print(f'{Prefix}> Fetching {url}')
        os.system(f'curl --tlsv1.3 https://dndtools.net{url} -o {fname}')


def fetch_index(Category, Book, Page):
    if DEBUG:
        print('------------------------------------------------------------------')
    URL = f'/{Category}/{Book}/?page={Page}'
    fName = f'{STORE}/{Category}/{Book}/index-{Page}.html'
    fetch(URL, fName, Prefix='===============')


def fetch_and_parse_index(Category, Slug, bookID, Page):
    Book = f'{Slug}--{bookID}'
    if not os.path.exists(f'{STORE}/{Category}'):
        os.mkdir(f'{STORE}/{Category}')
    if not os.path.exists(f'{STORE}/{Category}/{Book}'):
        os.mkdir(f'{STORE}/{Category}/{Book}')
    #
    fetch_index(Category, Book, Page)
    fName = f'{STORE}/{Category}/{Book}/index-{Page}.html'
    with open(fName) as ipf:
        soup=BeautifulSoup(ipf, 'html.parser')
    table = soup.table
    for row in table.find_all('tr'):
        cells = row.find_all('td')
        if len(cells) < 8:
            if DEBUG:
                print('Skipping Row')
        else:
            url = cells[0].a['href']
            fName = f'{STORE}{url[:-1]}.html'
            fetch(url, fName)
            parse_spell_page(bookID,fName)


def parse_spell_page(book_ID, fName):
    spell_ID=fName[:-5].split('--')[-1]
    Spell = {'Level':''}
    with open(fName) as ipf:
        soup = BeautifulSoup(ipf, 'html.parser')
    content=soup.find(id='content')
    #print(content)
    #print('-------------------------------------------------------------------')
    Spell['Title'] = content.h2.text
    try:
        for txt in content.text.split('\n'):
            for key in ['Casting Time:','Range:','Area:','Effect:','Duration:','Saving Throw:']:
                if txt.startswith(key):
                    if DEBUG:
                        print('Found: '+txt)
                    val = txt.split(':')[1]
                    k = key[:-1].split(' ')[0]
                    Spell[k]=val.strip()
                    if DEBUG:
                        print(f'Got {key} {val}')
        #
        Links = content.find_all('a')
        Levels = []
        for link in Links:
            for key in ['Bard', 'Cleric', 'Druid', 'Paladin', 'Ranger', 'Wizard']:
                if key in link.text and len(link.text)<12:
                    Levels.append(link.text)
        Spell['Level'] = ','.join(Levels)

    except:
        print('*********************   ERROR   ******************************')
        print(content.text)
        print(Spell)
    words = [str(p) for p in content.find_all('p')]
    Spell['Words']='\n'.join(words)
    # Save
    if not os.path.exists(f'spells/{book_ID}'):
        os.mkdir(f'spells/{book_ID}')
    opfName = f'spells/{book_ID}/{spell_ID}.json'
    if DEBUG:
        print('Saving to '+opfName)
    with open(opfName, 'w') as opf:
        json.dump( Spell, opf, indent=0)
    slug = fName.split('/')[-1].split('--')[0]
    Contents[spell_ID] = {'slug':slug, 'Title':Spell['Title']}

###
def fetch_a_book_category(Category):
    global Contents
    for bookID, Book in Ark.RuleBooks.items():
        #print(bookID)
        Contents = {}
        Slug = Book['slug']
        for idx in range(Book['index-pages']):
            fetch_and_parse_index(Category, Slug, bookID, idx+1)
        # Contents
        with open(f'spells/{bookID}/contents.json','w') as opf:
            json.dump( Contents, opf, indent=2 )


fetch_a_book_category('spells')
