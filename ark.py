from bs4 import BeautifulSoup
import json
import os
import requests

requests.packages.urllib3.disable_warnings()


class TheArk():
    WebSite0='http://dnd.arkalseif.info/'
    WebSite='https://dndtools.net/'
    All_Spells_Loaded = False

    Editions = {
      1: {'slug':'core-35', 'Title':'Core (3.5)'},
      5: {'slug':'supplementals-35', 'Title':'Supplementals (3.5)'},
    }

    RuleBooks = {
      6: { 'slug': 'players-handbook-v35', 'edition':1, 'Title':"Player's Handbook v.3.5", 'index-pages':31, 'spells':{} },
      55: { 'slug': 'complete-arcane', 'edition':5, 'Title':"Complete Arcane", 'index-pages':8, 'spells':{} },
      56: { 'slug': 'complete-divine', 'edition':5, 'Title':"Complete Divine", 'index-pages':7, 'spells':{} },
      86: { 'slug': 'spell-compendium', 'edition':5, 'Title':"Spell Compendium", 'index-pages':51, 'spells':{} },
    }

    @staticmethod
    def fetch_and_parse(book_ID, spell_ID):
        page = fetch_page(book_ID, spell_ID)
        if page.status_code == 200:
            try:
                Spell = parse_page(page)
                #print('Saving ...')
                with open(f'spells/{book_ID}/{spell_ID}.json', 'w') as opf:
                    json.dump( Spell, opf, indent=2)
            except:
                print(page.content)


    @staticmethod
    def fetch_page(book_ID, spell_ID):
        print(f'About to fetch book.{book_ID} / spell.{spell_ID}')
        URL=genSpellURL(book_ID, spell_ID)
        return requests.get(URL, verify=False)


    @staticmethod
    def genSpellURL(book_ID, spell_ID):
        book=Ark.RuleBooks[int(book_ID)]
        bookName=book['slug']
        spell=book['spells'][spell_ID]
        spellSlug=spell['slug']
        URL=f'{Ark.WebSite}spells/{bookName}--{book_ID}/{spellSlug}--{spell_ID}/'
        return URL


    def load_Contents(self):
        for book_ID,book in self.RuleBooks.items():
            if len(book['spells']) == 0:
                with open(f'spells/{book_ID}/contents.json', 'r') as ipf:
                    book['spells'] = json.load(ipf)
            #else:
            #    print(f'Skip load contents of {book_ID}')


    def load_Spell(self, book_ID, spell_ID):
        self.load_Contents()
        book = self.RuleBooks[book_ID]
        if 'URL' in book['spells'][spell_ID]:
            pass
            #print(f'Skipping load of {book_ID}/{spell_ID}')
        else:
            book['spells'][spell_ID]['URL'] = self.genSpellURL(book_ID, spell_ID)
            fname = f'spells/{book_ID}/{spell_ID}.json'
            if os.path.isfile(fname):
                with open(fname, 'r') as ipf:
                    spell_info = json.load(ipf)
                book['spells'][spell_ID].update(spell_info)
        return book['spells'][spell_ID]


    def load_All_Spells(self):
        if self.All_Spells_Loaded:
            pass
            #print('Skipping load ALL Spells')
        else:
            for book_ID,book in self.RuleBooks.items():
                for spell_ID,spell in book['spells'].items():
                    self.load_Spell(book_ID, spell_ID)
            self.All_Spells_Loaded = True
        return self.RuleBooks

###############################################################################
Ark = TheArk()
