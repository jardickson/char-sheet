import json
from pprint import pprint

from flask import request

from ark import Ark
from rules import Rules #parseCharSheet

AbilityOrder = ['STR','DEX','CON','INT','WIS','CHA']

### ---------------------------------------------------------------------------
def calcMySpellLevel(cSheet, Levels):
    scc = str(cSheet['Physical'].get('spellcaster'))
    for level in Levels.split(','):
        #print(f'Is {scc} in {level}')
        if scc in level:
            return level.split(' ')[1]
    return '??'


def genContext(cSheet, pgTitle=None):
    if cSheet:
        hdrCSS = 'bg-%s text-white px-1 py-0' % cSheet.Config['boot_header']
        cName = cSheet.Physical['title'] +' '+ cSheet.Physical['name']
        return {'pageTitle':cName, 'bsTheme':cSheet.Config['bootswatch'], 'abilityOrder':AbilityOrder,
            'cardHeader':hdrCSS, 'Ark':Ark, 'CS': cSheet}
    else:
        return {'pageTitle':pgTitle, 'bsTheme':'', 'CS': {}}


class CharSheet:
    __Data = {}

    def __init__(self, charName, Parse=False):
        self.Canonical = charName
        with open(f'characters/{charName}.json', 'r') as ipf:
            self.__Data = json.load(ipf)
        # Fixup
        Data = self.__Data
        if 'Config' not in Data:
            Data['Config'] = {}
        # Move attribs to Config
        for attrib in ['avatar', 'bootswatch', 'boot_header', 'campaign', 'show_in_use']:
            if attrib in Data:
                self.Config[attrib] = Data[attrib]
                Data.pop(attrib)
        # Move attribs to Physical
        for attrib in ['level', 'name', 'title', 'xp']:
            if attrib in Data:
                self.Physical[attrib] = Data[attrib]
                Data.pop(attrib)
        # Parse
        if Parse:
            self.parse()


    def __getitem__(self, key):
        return self.__Data[key]


    def save(self):
        cn = self.Canonical
        with open(f'characters/{cn}.json', 'w') as opf:
            json.dump( self.__Data, opf, indent=2, sort_keys=True)


    @property
    def Abilities(self):
        return self.__Data['Abilities']

    @property
    def Armour(self):
        # From the inventory
        return self.__Data['Armour']

    @property
    def Combat(self):
        return self.__Data['Combat']

    @property
    def Config(self):
        return self.__Data['Config']

    @property
    def Feats(self):
        return self.__Data['Feats']

    @property
    def Inventory(self):
        return self.__Data['Inventory']

    @property
    def Money(self):
        return self.__Data['Money']

    @property
    def Notes(self):
        return self.__Data['Notes']

    @property
    def Physical(self):
        return self.__Data['Physical']

    @property
    def Saving(self):
        return self.__Data['Saving']

    @property
    def Skills(self):
        return self.__Data['Skills']

    @property
    def Spellbooks(self):
        return self.__Data['Spellbooks']

    @property
    def Weapons(self):
        # From the inventory
        return self.__Data['Weapons']


    ### Apply all the Rules
    def parse(self):
        cs = self.__Data
        A = self.Abilities
        C = self.Combat
        F = self.Feats
        Inv = self.Inventory
        P = self.Physical
        ST = self.Saving

        # Physical
        P['xp2'] = int(pow(2,P['level']) * 500)

        # Quick run through the INV to apply any bonuses for In-Use items
        for Item in Inv:
            if Item['in_use']:
                for ability in AbilityOrder:
                    bonusName = 'bonus_'+ability.lower()
                    if bonusName in Item:
                        A[ability]['bonus'] = A[ability].get('bonus',0) + Item[bonusName]
                if 'bonus_init' in Item:
                    C['init_bonus'] += Item['bonus_init']
                for save in ST:
                    bonusName = 'bonus_'+save['name'].lower()
                    if bonusName in Item:
                        save['bonus'] = Item[bonusName] + save.get('bonus',0)

        # Abilities
        for aa in A:
            ability = A[aa]
            ability['score'] = ability['base'] + ability['temp'] + ability.get('bonus',0)
            ability['mod'] = Rules.calcAbilityMod(ability['score'])
            ability['bonus'] = 0
            if ability['temp'] < 0:
                ability['css'] = 'danger'
            if ability['temp'] > 0:
                ability['css'] = 'success'

        # Inventory
        cs['Armour'] = {'gear': [], 'total':0}
        cs['Weapons'] = []
        for idx in range(len(Inv)):
            Item = Inv[idx]
            Item['inv_index'] = idx
            if Item['type'] == 'Armour':
                Item['icon']='fas fa-shield-alt text-warning'
                if Item['in_use']:
                    Rules.updateArmour(Item)
                    cs['Armour']['gear'].append(Item)
                    cs['Armour']['total'] += Item['armour'] + Item['bonus']
            if Item['type'] == 'General':
                Item['icon']='fas fa-icons text-primary'
                if Item['in_use']:
                    cs['Armour']['gear'].append(Item)
            if Item['type'] == 'Consumable':
                Item['icon']='fas fa-flask text-info'
            if Item['type'] == 'Natural':
                Item['icon']='fas fa-user text-success'
            if Item['type'] == 'Scroll':
                Item['icon']='fas fa-scroll text-warning'
            if Item['type'] == 'Weapon':
                Item['icon']='fas fa-fist-raised text-danger'
                if Item['in_use']:
                    Rules.updateWeapon(Item, A['STR']['mod'])
                    # Which Ability gets added
                    abilityMod = 0
                    if Item['style'] == 'melee':
                        abilityMod = A['STR']['mod']
                        Item['abilityMod'] = f'STR={abilityMod}'
                    if Item['style'] == 'ranged':
                        abilityMod = A['DEX']['mod']
                        Item['abilityMod'] = f'DEX={abilityMod}'
                    toHit = [ str(aa + Item['bonus'] + abilityMod) for aa in C['all_attacks'] ]
                    Item['to_hit'] = ' / '.join(toHit)
                    cs['Weapons'].append(Item)

        # Combat
        C['grapple'] = cs['Combat']['base_attack'] + cs['Abilities']['STR']['mod']
        C['AC'] = 10 + A['DEX']['mod'] + cs['Armour']['total']
        C['initiative'] = A['DEX']['mod'] + C['init_bonus']

        # Skills
        skillPoints = 0
        for ss in range(len(cs['Skills'])):
            skill = cs['Skills'][ss]
            Rules.updateSkill(skill)
            skillPoints += skill['score']
            skill['mod'] = cs['Abilities'][skill['key']]['mod']
            if skill['score'] or skill['UT']:
                skill['css'] = 'success'
                skill['total'] = skill['score'] + skill['mod'] + skill['misc']
            else:
                skill['css'] = ''
                skill['total'] = '-'
        cs['skill_points'] = skillPoints

        # Feats
        for feat in F:
            Rules.updateFeat(feat)

        # Money
        ttl = 0
        for cc in cs['Money']['currency']:
            ttl += cc['qty'] * cc['factor']
        cs['Money']['total'] = ttl

        # Spells
        Ark.load_Contents()
        for book in cs['Spellbooks']:
            for tag, spell in book['spells'].items():
                bid=spell['book_id']
                sid=spell['spell_id']
                arkSpell = Ark.load_Spell(bid,str(sid))
                #
                spell['myLevel'] = calcMySpellLevel(cs, arkSpell['Level'])

        # Saving
        for save in cs['Saving']:
            save['score'] = save['base']
            save['calc'] = 'Base of %d' % save['base']
            if 'ability' in save:
                a = cs['Abilities'][save['ability']]['mod']
                save['score'] += a
                save['calc'] += ' + %s (%d)' % (save['ability'],a)
            if 'bonus' in save:
                save['score'] += save['bonus']
                save['calc'] += ' + %s (bonus)' % (save['bonus'])


    # Make updates (changes) after edits
    def update(self, Section, ID, reqForm):
        cSheet = self.__Data
        #pprint(reqForm)

        # -----------------------------------------------------------
        if Section == 'Abilities':
            for ab in cSheet['Abilities']:
                cSheet['Abilities'][ab]['temp'] = int(reqForm.get(ab))
        # -----------------------------------------------------------
        if Section == 'Combat':
            cSheet['Combat']['init_bonus'] = int(reqForm.get('init_bonus'))
            cSheet['Combat']['max_hp'] = int(reqForm.get('max_hp'))
            aaa = reqForm.get('all_attacks').split('/')
            for aa in range(len(aaa)):
                aaa[aa] = int(aaa[aa])
            cSheet['Combat']['all_attacks'] = aaa
            cSheet['Combat']['base_attack'] = aaa[0]
        # -----------------------------------------------------------
        if Section == 'Physical':
            P=cSheet['Physical']
            P['race'] = reqForm.get('race')
            P['class'] = reqForm.get('class')
            P['spellcaster'] = reqForm.get('spellcaster')
            P['alignment'] = reqForm.get('alignment')
            P['gender'] = reqForm.get('gender')
            P['height'] = reqForm.get('height')
            P['level'] = int(reqForm.get('level'))
            P['xp'] += int(reqForm.get('add_xp'))
        # -----------------------------------------------------------
        if Section == 'Feats':
            if ID < 0:
                cSheet['Feats'].append({})
                ID = len(cSheet['Feats']) - 1
            if request.method == 'DELETE':
                cSheet['Feats'].pop(ID)
            else: # Update
                item = cSheet['Feats'][ID]
                item['name'] = reqForm.get('name')
                item['notes'] = reqForm.get('notes')
        # -----------------------------------------------------------
        if Section == 'Skills':
            for key in reqForm:
                if key[:5] == 'skill':
                    idx = int(key[6:])
                    val = reqForm.get(key)
                    cSheet['Skills'][idx]['suffix'] = val
        # -----------------------------------------------------------
        if Section == 'Inventory':
            if ID < 0: # If new, then add 'blank'
                cSheet['Inventory'].append({'in_use':False})
                ID = len(cSheet['Inventory']) - 1
            if request.method == 'DELETE':
                cSheet['Inventory'].pop(ID)
            else: # Update
                item = cSheet['Inventory'][ID]
                item['prefix'] = reqForm.get('prefix')
                item['name'] = reqForm.get('name')
                item['suffix'] = reqForm.get('suffix')
                item['type'] = reqForm.get('type')
                item['magical'] = reqForm.get('magical') == 'on'
                try:
                    item['bonus'] = int(reqForm.get('bonus'))
                except:
                    item['bonus'] = 0
                try:
                    item['qty'] = int(reqForm.get('qty'))
                except:
                    item['qty'] = 1
                for suffix in ['ac','str','dex','con','int','wis','cha','init','fortitude','reflex','will','poison','spells']:
                    bonus = 'bonus_'+suffix
                    try:
                        b = int(reqForm.get(bonus))
                        if b:
                            item[bonus] = b
                        else:
                            item.pop(bonus,None)
                    except:
                        item.pop(bonus,None)
                item['notes'] = reqForm.get('notes')
                print(item)
        # -----------------------------------------------------------
        if Section == 'Money':
            for cc in cSheet['Money']['currency']:
                cc['qty'] = int(reqForm.get(cc['name']))
        # -----------------------------------------------------------
        if Section == 'Saving':
            for save in cSheet['Saving']:
                save['base'] = int(reqForm.get(save['name']))
        # -----------------------------------------------------------
        if Section == 'Spellbooks':
            if request.method == 'DELETE':
                cSheet['Spellbooks'].pop(ID)
            else:
                sbTitle = reqForm.get('sbTitle')
                if ID < 0:
                    cSheet['Spellbooks'].append({'title':sbTitle,'spells':{}})
                else:
                    cSheet['Spellbooks'][ID]['title'] = sbTitle
