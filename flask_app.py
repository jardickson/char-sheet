
# A very simple Flask Hello World app for you to get started with...

from math import pow
from pprint import pprint
from flask import Flask, redirect, request, render_template as render, send_file

from csheet import *
from lib import *
import os
from ark import Ark
from rules import Rules

class myConfig(object):
    DEBUG = True
    HOST = '0.0.0.0'
    POST = 5001

app = Flask(__name__)

### ---------------------------------------------------------------------------

@app.route('/')
def home():
    context = genContext(None, 'Home')
    context['Characters'] = []
    charList = os.listdir('characters')
    charList.sort()
    for fName in charList:
        if fName[-4:] == 'json' and fName[0] != '_':
            context['Characters'].append( CharSheet(fName[:-5]) )
    return render('home.html', **context)


@app.route('/hello')
def hello_world():
    d20 = requests.get('https://www.d20srd.org/srd/spells/burningHands.htm')
    return 'Hello World from Flask (via git) !\n'


@app.route('/admin/magic')
def admin_magic():
    context = genContext(None, 'Magic')
    Ark.load_Contents()
    Ark.load_All_Spells()
    context['Books'] = Ark.RuleBooks
    return render(f'magic.html', **context)


@app.route('/ark/get/<book_ID>/<spell_ID>')
def ark_get(book_ID, spell_ID):
    Ark.fetch_and_parse(book_ID, spell_ID)
    return redirect('/admin/magic')


@app.route('/download/<charName>', methods=['GET'])
def download_sheet(charName):
    cSheet = loadCharSheet(charName)
    return send_file(f'characters/{charName}.json')


@app.route('/export/<charName>', methods=['GET'])
def export_sheet(charName):
    cSheet = loadCharSheet(charName)
    # Export it
    from reportlab.pdfgen import canvas
    c = canvas.Canvas(f'characters/{charName}.pdf')
    c.drawString(5,5, 'Hello EMON')
    c.line(480,747,580,747)
    c.save()
    # Then send it
    return send_file(f'characters/{charName}.pdf')


@app.route('/help/<helpPage>')
def help(helpPage):
    context = genContext(None, 'Help : '+helpPage)
    return render(f'help_{helpPage}.html', **context)


@app.route('/test', methods=['GET', 'POST'])
def test_browser():
    print(app.config)

    context = genContext(None, 'Browser Test')
    if request.method == 'POST':
        context['Test2'] = request.form.get('test2')
    return render(f'test.html', **context)


@app.route('/test2', methods=['GET', 'POST'])
def test_browser2():
    context = genContext(None, 'Browser Test (Full)')
    if request.method == 'POST':
        context['Test2'] = request.form.get('test2')
    return render(f'test2.html', **context)


@app.route('/static/<fname>')
def get_static(fname):
    mt = 'image'
    if fname[-3:] == 'css':
        mt = 'text/css'
    if fname[-2:] == 'js':
        mt = 'text/javascript'
    return send_file('static/'+fname, mimetype=mt)


@app.route('/cs/<charName>/<section>', methods=['GET'])
def get_section(charName, section):
    cSheet = CharSheet(charName, Parse=True)
    context = genContext(cSheet)
    html = render(f'_{section}.htm', **context)
    return html


@app.route('/cs/<charName>', methods=['GET'])
def get_sheet(charName):
    cSheet = CharSheet(charName, Parse=True)
    context = genContext(cSheet)
    html = render('csheet.html', **context)
    return html


@app.route('/test/<Section>/<ID>/<Attrib>', methods=['PATCH'])
@app.route('/cs/<charName>/<Section>/<ID>/<Attrib>', methods=['PATCH','DELETE'])
def patch_section(charName='test', Section='', ID=0, Attrib=''):
    Json = request.form
    #pprint(Json)
    # Load current
    cSheet = CharSheet(charName)

    # Amend ?
    if Section=='Inventory' and Json['value'] == 'toggle':
        a = cSheet[Section][int(ID)][Attrib]
        cSheet[Section][int(ID)][Attrib] = not a
    if Section=='Combat' and Attrib!='nonce':
        val = int('0'+Json['value'])
        if Attrib == 'Damage':
            cSheet['Combat']['damage'] += val
        if Attrib == 'Healing':
            cSheet['Combat']['damage'] -= val
            if cSheet['Combat']['damage'] < 0:
                cSheet['Combat']['damage'] = 0
    if Section=='Skills':
        delta = int(Json['value'])
        print(f'S.{ID} {delta}')
        cSheet['Skills'][int(ID)]['score'] += delta
    if Section=='Spellbooks':
        # Delete ?
        if request.method == 'DELETE':
            cSheet['Spellbooks'][int(ID)]['spells'].pop(Attrib)
        else:
            if Attrib in ['memorized', 'shredded']:
                sid = request.args.get('item')
                if Attrib in cSheet['Spellbooks'][int(ID)]['spells'][sid]:
                    cSheet['Spellbooks'][int(ID)]['spells'][sid][Attrib] = not cSheet['Spellbooks'][int(ID)]['spells'][sid][Attrib]
                else:
                    cSheet['Spellbooks'][int(ID)]['spells'][sid][Attrib] = True
    # Now Save
    cSheet.save()
    # Go and show
    cSheet.parse()
    context = genContext(cSheet)
    html = render(f'_{Section}.htm', **context)
    return html


@app.route('/test/<Section>', methods=['DELETE','POST'])
@app.route('/cs/<charName>/<Section>', methods=['DELETE','POST'])
@app.route('/cs/<charName>/<Section>/<ID>', methods=['DELETE','POST'])
def post_section(charName='test', Section='', ID=0):
    # Load current
    cSheet = CharSheet(charName)
    # Go and make any Updates
    cSheet.update(Section, int(ID), request.form)
    # Save & return the section
    cSheet.save()
    #
    cSheet.parse()
    context = genContext(cSheet)
    html = render(f'_{Section}.htm', **context)
    #html = render('csheet.html', **context)
    return html


@app.route('/cs/<charName>/<Section>/<ID>/<Item>', methods=['POST'])
def post_SectionItem(charName, Section, ID, Item):
    # Load current
    cSheet = loadCharSheet(charName)
    # Go and make any Updates
    if Section == 'Spellbooks':
        print(f'Adding {Item} to {ID}')
        book_ID,spell_ID = Item.split(':')
        Spell = Ark.load_Spell(int(book_ID), spell_ID)
        lvl = calcMySpellLevel(cSheet, Spell['Level'])
        tag = f'{lvl}:{Spell["Title"]}'
        print(tag)
        cSheet['Spellbooks'][int(ID)]['spells'][tag] = {'book_id':int(book_ID), 'spell_id':int(spell_ID)}
    # Save & return the section
    saveCharSheet( charName, cSheet )
    #
    parseCharSheet(cSheet)
    context = genContext(cSheet)
    html = render(f'_{Section}.htm', **context)
    #html = render('csheet.html', **context)
    return html


@app.route('/cs/<charName>', methods=['POST'])
def post_Sheet(charName):
    reqForm = request.form
    #pprint(reqForm)
    formID = reqForm.get('formID')
    # Load current
    cSheet = CharSheet(charName)
    # Amend
    # -----------------------------------------------------------
    if formID == 'Configure':
        cSheet.Physical['title'] = reqForm.get('title')
        t = reqForm.get('theme')
        if t=='default':
          t = ''
        cSheet.Config['bootswatch'] = t
        cSheet.Config['boot_header'] = reqForm.get('header')
        cSheet.Config['show_in_use'] = reqForm.get('show_in_use') == 'on'
    # Now Save
    cSheet.save()
    # Go and show
    return redirect(request.path)


@app.route('/rules/Basic')
def rules_Basic(rulesPage='Basic'):
    context = genContext(None, 'Rules : '+rulesPage)
    context['Rules'] = Rules
    return render(f'rules_{rulesPage}.html', **context)


@app.route('/rules/Magic', methods=['GET','POST'])
def rules_Magic():
    if request.method == 'GET':
        return redirect(request.path+'/6/A')
    # Do a search
    Term = request.form.get('term').lower()
    context = genContext(None, 'Rules : Magic Search')
    context['RuleBooks'] = Ark.RuleBooks
    for BID, Book in Ark.RuleBooks.items():
        Book['spells'] = {}
        with open(f'spells/{BID}/contents.json') as ipf:
            Contents = json.load(ipf)
        for SID, Spell in Contents.items():
            if Term in Spell['Title'].lower():
                with open(f'spells/{BID}/{SID}.json') as ipf:
                    data = json.load(ipf)
                Spell.update(data)
                Ark.RuleBooks[BID]['spells'][SID] = Spell

    return render('rules_Magic_Srch.html', **context)

@app.route('/rules/Magic/<Book>')
@app.route('/rules/Magic/<Book>/<Item>')
def rules_Magic_Book(Book, Item='A'):
    context = genContext(None, 'Rules : Magic')
    with open(f'spells/{Book}/contents.json') as ipf:
        Contents = json.load(ipf)
    Initials = {}
    for aa in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
        Initials[aa] = 0
    for idx, spell in Contents.items():
        initial = spell['Title'][0]
        Initials[initial] += 1
        if initial == Item:
            with open(f'spells/{Book}/{idx}.json') as ipf:
                data = json.load(ipf)
            spell.update(data)
    #
    context['Title'] = Ark.RuleBooks[int(Book)]['Title']
    context['Initial'] = Item
    context['Initials'] = Initials
    context['RuleBooks'] = Ark.RuleBooks
    context['RuleBook'] = Ark.RuleBooks[int(Book)]
    context['RuleBook']['spells'] = Contents
    return render(f'rules_Magic.html', **context)


@app.route('/search/spells/<SrchTerm>')
def search_Spells(SrchTerm):
    Terms = SrchTerm.lower().split(',')
    Ark.load_Contents()
    Spells=[]
    for BID, Book in Ark.RuleBooks.items():
        bName = Book['Title']
        for SID, Spell in Book['spells'].items():
            for term in Terms:
                if term in Spell['Title'].lower():
                    sn = Spell['Title']
                    print(f'Found {sn}')
                    Spells.append(f'<tr><td>{sn}</td><td>{bName}</td><td><a onClick="addSpell2book({BID},{SID});" class="btn btn-success py-1">Add</a></td></tr>')
    return '\n'.join(Spells)
