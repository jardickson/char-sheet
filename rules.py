
FEATS = {
  'Favoured Enemy': { 'url': 'https://www.dandwiki.com/wiki/SRD:Ranger#Favored_Enemy',
    'description': """At 1st level, a ranger may select a type of creature -
the ranger gains a +2 bonus on Bluff, Listen, Sense Motive, Spot, and Survival checks.<br>
Likewise, he gets a +2 bonus on weapon damage rolls against such creatures.<br>
At 5th level (10th, 15th, and 20th level), the ranger may select an additional favored enemy. In addition, at each such interval, the bonus against any one favored enemy (including the one just selected, if so desired) increases by 2
    """ },
  'Track': {'url': 'https://www.dandwiki.com/wiki/SRD:Ranger#Track',
    'description': """Lorum<br>Ipsum
     """},
  'Combat Style': {'url': 'https://www.dandwiki.com/wiki/SRD:Ranger#Combat_Style',
    'description': """Lorum Ipsum
     """},
  'Endurance': {'url': 'https://www.dandwiki.com/wiki/SRD:Ranger#Endurance',
    'description': """Lorum Ipsum
     """},
  'Animal Companion': {'url': 'https://www.dandwiki.com/wiki/SRD:Ranger#Animal_Companion',
    'description': """Lorum Ipsum
     """},
  'Improved Combat Style': {'url': 'https://www.dandwiki.com/wiki/SRD:Ranger#Improved_Combat_Style',
    'description': """Lorum Ipsum
     """},
  'Weapon Focus': {'url': 'https://www.dandwiki.com/wiki/SRD:Ranger#Weapon_Focus',
    'description': """Lorum Ipsum
     """},
  'Two-Weapon Fighting': {'url': 'https://www.dandwiki.com/wiki/SRD:Ranger#',
    'description': """
     """},
  'Precise Shot': {'url': 'https://www.dandwiki.com/wiki/SRD:Ranger#',
    'description': """Allows Into Melee
     """},
  'Point Blank Shot': {'url': 'https://www.dandwiki.com/wiki/SRD:Point_Blank_Shot',
    'description': """Allows close combat
     """},
  'Rapid Shot': {'url': 'https://www.dandwiki.com/wiki/SRD:Rapid_Shot',
    'description': """
     """},
  'Many Shot': {'url': 'https://www.dandwiki.com/wiki/SRD:ManyShot',
    'description': """
     """},
  'Swift Tracker': {'url': 'https://www.dandwiki.com/wiki/SRD:Ranger#Swift_Tracker',
    'description': """Lorum Ipsum
     """},
  'Wild Empathy': {'url': 'https://www.dandwiki.com/wiki/SRD:Ranger#Wild_Empathy',
    'description': """Lorum Ipsum
     """},
  'Woodland Stride': {'url': 'https://www.dandwiki.com/wiki/SRD:Ranger#Woodland_Stride',
    'description': """Lorum Ipsum
     """},
}

ARMOUR = { # Must be Title-Caps
    'Padded': {'armour':1, 'description':'' },
    'Leather': {'armour':2, 'description':'' },
    'Studded Leather': {'armour':3, 'description':'' },
    'Chain Shirt': {'armour':4, 'description':'' },
    'Hide': {'armour':3, 'description':'' },
    'Scale Mail': {'armour':4, 'description':'' },
    'Chainmail': {'armour':4, 'description':'' },
    'Breastplate': {'armour':5, 'description':'' },
    'Split Mail': {'armour':5, 'description':'' },
    'Banded Mail': {'armour':6, 'description':'' },
    'Half Plate': {'armour':7, 'description':'' },
    'Full Plate': {'armour':8, 'description':'' },
    'Buckler': {'armour':1, 'description':'' },
    'Light Steel Shield': {'armour':1, 'description':'' },
    'Light Wooden Shield': {'armour':1, 'description':'' },
    'Heavy Steel Shield': {'armour':2, 'description':'Round :)' },
    'Heavy Wooden Shield': {'armour':2, 'description':'Round :)' },
}
WEAPONS = {
    # Ranged
    'Crossbow': { 'style':'ranged', 'damage':'d8', 'critical':'@19x2', 'description':''},
    'Heavy Crossbow': { 'style':'ranged', 'damage':'d10', 'critical':'@19x2', 'description':''},
    'Light Crossbow': { 'style':'ranged', 'damage':'d8', 'critical':'@19x2', 'description':''},
    'Longbow': { 'style':'ranged', 'damage':'d8', 'critical':'@20x3', 'description':''},
    'Repeating Heavy Crossbow': { 'style':'ranged', 'damage':'d10', 'critical':'@19x2', 'description':''},
    'Repeating Light Crossbow': { 'style':'ranged', 'damage':'d8', 'critical':'@19x2', 'description':''},
    'Shortbow': { 'style':'ranged', 'damage':'d6', 'critical':'@20x3', 'description':''},
    # Melee
    'Battleaxe': { 'style':'melee', 'damage':'d8', 'critical':'@20x3', 'description':''},
    'Barruck Polearm': { 'style':'melee', 'damage':'2d8', 'critical':'@20x2', 'description':'Min STR.18 to wield'},
    'Dagger': { 'style':'melee', 'damage':'d4', 'critical':'@19x2', 'description':''},
    'Dwarven Waraxe': { 'style':'melee', 'damage':'d10', 'critical':'@20x3', 'description':''},
    'Greateaxe': { 'style':'melee', 'damage':'d12', 'critical':'@20x3', 'description':''},
    'Greatsword': { 'style':'melee', 'damage':'2d6', 'critical':'@19x2', 'description':'Two-handed = 1.5 STR bonus'},
    'Longsword': { 'style':'melee', 'damage':'d8', 'critical':'@19x2', 'description':''},
    'Shortsword': { 'style':'melee', 'damage':'d6', 'critical':'@19x2', 'description':''},
    'Dwarven Axe': { 'style':'melee', 'damage':'d10', 'critical':'@20x3', 'description':''},
}
SKILLS = {
    'Appraise':         {'UT':1, 'key':'INT'},
    'Balance':          {'UT':1, 'key':'DEX'},
    'Bluff':            {'UT':1, 'key':'CHA'},
    'Climb':            {'UT':1, 'key':'STR'},
    'Concentration':    {'UT':1, 'key':'CON'},
    'Craft':            {'UT':1, 'key':'INT'},
    'Decipher Script':  {'UT':0, 'key':'INT'},
    'Diplomacy':        {'UT':1, 'key':'CHA'},
    'Disable Device':   {'UT':0, 'key':'INT'},
    'Disguise':         {'UT':1, 'key':'CHA'},
    'Escape Artist':    {'UT':1, 'key':'DEX'},
    'Forgery':          {'UT':1, 'key':'INT'},
    'Gather Information':   {'UT':1, 'key':'CHA'},
    'Handle Animal':    {'UT':0, 'key':'CHA'},
    'Heal':             {'UT':1, 'key':'WIS'},
    'Hide':             {'UT':1, 'key':'DEX'},
    'Intimidate':       {'UT':1, 'key':'CHA'},
    'Jump':             {'UT':1, 'key':'STR'},
    'Knowledge':        {'UT':0, 'key':'INT'},
    'Listen':           {'UT':1, 'key':'WIS'},
    'Move Silently':    {'UT':1, 'key':'DEX'},
    'Open Lock':        {'UT':0, 'key':'DEX'},
    'Perform':          {'UT':0, 'key':'CHA'},
    'Profession':       {'UT':0, 'key':'WIS'},
    'Ride':             {'UT':1, 'key':'DEX'},
    'Search':           {'UT':1, 'key':'INT'},
    'Sense Motive':     {'UT':0, 'key':'WIS'},
    'Sleight of Hand':  {'UT':1, 'key':'DEX'},
    'SpellCraft':       {'UT':0, 'key':'INT'},
    'Spot':             {'UT':1, 'key':'WIS'},
    'Survival':         {'UT':1, 'key':'WIS'},
    'Swim':             {'UT':1, 'key':'STR'},
    'Tumble':           {'UT':0, 'key':'DEX'},
    'Use Magic Device': {'UT':0, 'key':'CHA'},
    'Use Rope':         {'UT':1, 'key':'DEX'},
    '':                 {'UT':0, 'key':'INT'},
    'Misc':             {'UT':0, 'key':'INT'},
}

class RULES:
    @staticmethod
    def calcAbilityMod(abScore):
        return int(abScore / 2) - 5

    @staticmethod
    def getArmour():
        return ARMOUR

    @staticmethod
    def getFeats():
        return FEATS

    @staticmethod
    def getSkills():
        return SKILLS

    @staticmethod
    def getWeapons():
        return WEAPONS

    @staticmethod
    def updateArmour(invItem):
        name = invItem['name'].title()
        if name in ARMOUR:
            invItem.update(ARMOUR[name])
        else:
            invItem.update({'armour':0, 'description':''})

    @staticmethod
    def updateFeat(Feat):
        name = Feat['name']
        if name in FEATS:
            Feat.update(FEATS[name])
        else:
            Feat.update({'description':'Unknown'})

    @staticmethod
    def updateSkill(Skill):
        name = Skill['name']
        if name in SKILLS:
            Skill.update(SKILLS[name])
        else:
            Skill.update({'UT':0, 'key':'???', 'notes':'###'})

    @staticmethod
    def updateWeapon(invItem, STR_Modifier):
        name = invItem['name']
        invItem['dmg_bonus'] = invItem['bonus']
        if name in WEAPONS:
            invItem.update(WEAPONS[name])
            if WEAPONS[name]['style'] == 'melee':
                invItem['dmg_bonus'] += STR_Modifier
        else:
            invItem.update({'style':'Unknown', 'damage':0, 'description':''})
        invItem['full_name'] = ' '.join([invItem['prefix'],invItem['name'],invItem['suffix']]).strip().replace(' ,', ',')


###
Rules = RULES()
