// ----------------------------------------------------------------------------
$(function () {
  $('[data-toggle="popover"]').popover();
  $.ajaxSetup({ cache: false });
  console.log('Page loaded');
})

function useInv(idx, itemType){
    toggleAttrib('Inventory',idx,'in_use');
    if (itemType=='Armour' || itemType=='General') {
        setTimeout(function(){toggleAttrib('Gear',0,'nonce');}, 250);
    }
    if (itemType=='Weapon') {
        setTimeout(function(){toggleAttrib('Weapons',0,'nonce');}, 250);
    }
}

function unuse(invIdx, type){
    toggleAttrib('Inventory',invIdx,'in_use');
    setTimeout(function(){toggleAttrib(type,0,'nonce')}, 150);
}

// ----------------------------------------------------------------------------
function pokeAttrib(Section,ID,Attrib,Value) {
    if (Value == 'T_E_S_T') {
      URL='/cs/test/'+Section+'/'+ID+'/'+Attrib;
    } else {
      URL=location.pathname+'/'+Section+'/'+ID+'/'+Attrib;
    };
    if (Value == 'DELETE!') {
        $.ajax({type:'DELETE',
            url:URL,
            success:function(data){
                $('#div'+Section).html(data);
                $('[data-toggle="popover"]').popover();
            }
        });
    } else {
        $.ajax({type:'PATCH',
            url:URL,
            data:{'value':Value},
            success:function(data){
                $('#div'+Section).html(data);
                $('[data-toggle="popover"]').popover();
            }
        });
    }
}

// ----------------------------------------------------------------------------
function postSection(Section,ID,Attrib) {
    URL=location.pathname+'/'+Section+'/'+ID+'/'+Attrib;
    setTimeout( function(){
        $.ajax({type:'POST',
            url:URL,
            success:function(data){
                $('#div'+Section).html(data);
                $('[data-toggle="popover"]').popover();
            }
        });
    }, 150 );
}

// ----------------------------------------------------------------------------
function toggleAttrib(Section,ID,Attrib) {
    if (Attrib == 'T_E_S_T') {
      URL='/cs/test/'+Section+'/'+ID+'/nonce';
    } else {
      URL=location.pathname+'/'+Section+'/'+ID+'/'+Attrib;
    }
    $.ajax({type:'PATCH',
        url:URL,
        data:{'value':'toggle'},
        success:function(data){
            $('#div'+Section).html(data);
            $('[data-toggle="popover"]').popover();
        }
    });
}

// ----------------------------------------------------------------------------
function saveSectionItem(event){
    event.preventDefault();
    //console.log(event);
    FORM=event.target.id;
    SECTION=FORM.substr(2);
    URL=event.target.action+'/'+SECTION;
    //console.log(URL);
    ID=$('#'+FORM+' input[name="ID"]');
    if (ID.length == 1) { URL+='/'+ID.val() };
    $('#dlg'+SECTION).modal('hide');
    $('#body'+SECTION).html("<i class='far fa-hourglass fs200'></i>");

    ActElem=document.activeElement;
    console.log('Event: ',event);
    console.log('ActElem: ',ActElem);

    Data=$('#'+FORM).serialize();
    if (ActElem.hasAttribute('value')) {
        SubVal=ActElem.getAttribute('value');
        Data+='&'+ActElem.name+'='+SubVal;
    } else {
        SubVal='';
    }
    if (SubVal=='Delete') {
        setTimeout(function(){
            $.ajax({type:'DELETE', url:URL, data:Data, cache:false,
                success:function(data){
                    $('#div'+SECTION).html(data);
                    $('[data-toggle="popover"]').popover();
                }
            });
        }, 150);
    } else {
        setTimeout(function(){
            $.ajax({type:'POST', url:URL, data:Data, cache:false,
                success:function(data){
                    $('#div'+SECTION).html(data);
                    $('[data-toggle="popover"]').popover();
                    if (Data.includes('name=_Racial')){
                        console.log('Boom');
                        toggleAttrib('Abilities', 0, 'nonce');
                        toggleAttrib('Physical', 0, 'nonce');
                    }
                }
            });
        }, 150);
    };
};

// ----------------------------------------------------------------------------
// FEATS
function setFeatData(ID, Item){
  console.log('Edit Feat.'+ID);
  if (ID < 0) {
      Item = {"name":"New Feat", "notes":""};
      $('#fDelete').hide();
  } else { $('#fDelete').show(); }
  $('#fID').val(ID);
  $('#fName').val(Item.name);
  $('#fNotes').text(Item.notes);
}

// ----------------------------------------------------------------------------
// INVENTORY
function setInvData(ID, Item){
  console.log('Edit Inv.'+ID);
  if (ID < 0) {
      Item = {"prefix":"", "name":"New Item", "suffix":"", "type":"General", "bonus":0, "qty":1, "notes":""};
      $('#invDelete').hide();
  } else { $('#invDelete').show(); }
  $('#invID').val(ID);
  $('#fmInventory .modal-title').text('Edit '+Item.name+' Attributes');
  if (Item.name.startsWith('_')) {
    $('.inv-hide').addClass('d-none');
  } else {
    $('.inv-hide').removeClass('d-none');
  }
  $('#invPrefix').val(Item.prefix);
  $('#invName').val(Item.name);
  $('#invSuffix').val(Item.suffix);
  $('#invArmour').attr('selected', Item.type=='Armour');
  $('#invConsumable').attr('selected', Item.type=='Consumable');
  $('#invNatural').attr('selected', Item.type=='Natural');
  $('#invScroll').attr('selected', Item.type=='Scroll');
  $('#invWeapon').attr('selected', Item.type=='Weapon');
  $('#invMagical').attr('checked', Item.magical);
  $('#invBonus').val(Item.bonus);
  $('#invQty').val(Item.qty);
  $('#invNotes').text(Item.notes);

  $('#invBonus_str').val(Item.bonus_str);
  $('#invBonus_dex').val(Item.bonus_dex);
  $('#invBonus_con').val(Item.bonus_con);
  $('#invBonus_int').val(Item.bonus_int);
  $('#invBonus_wis').val(Item.bonus_wis);
  $('#invBonus_cha').val(Item.bonus_cha);
  $('#invBonus_init').val(Item.bonus_init);
  $('#invBonus_fortitude').val(Item.bonus_fortitude);
  $('#invBonus_reflex').val(Item.bonus_reflex);
  $('#invBonus_will').val(Item.bonus_will);
  $('#invBonus_poison').val(Item.bonus_poison);
  $('#invBonus_spells').val(Item.bonus_spells);
}

// ----------------------------------------------------------------------------
// SPELLS & BOOKS
function srch4spells2add(){
  term = $('#srch-term').val();
  if (term.length < 3) {
    alert('Please enter at least three characters');
  }else {
    $('#srch4spells').html("<i class='far fa-hourglass fs200'></i>");
    $.ajax({type:'GET',
        url:'/search/spells/'+term,
        success:function(data){
            $('#srch4spells').html(data);
            $('[data-toggle="popover"]').popover();
        }
    });

  }
};
// -------------------------------------
function addSpell2book(RuleBook,Spell){
  $('#dlgAddSpell .close').click();
  book=$('#sbID').val();
  postSection('Spellbooks',book,RuleBook+':'+Spell);
}
